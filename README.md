# Wardrobify

Team:

* Gina John - Shoes microservice
* Corey Schanz - Hats microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoe model includes name, manufacturer, model_name, color, and picture_url. We thought name was appropriate for the user to be able to have a better understanding of what shoe the app was referring to. The poller I used polled for data from the Bin Model in the wardrobe microservice to be able to relate the Shoe with the associated bin. Inside the BinVO, I requested closet_name, bin_number, bin_size, and the import_href. I used the bin_number variable in my POST request instead of href because I thought it would be better for the user experience to remember.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

We made a Hat model that has name, fabric, style name, color and a picture url. We added a location VO to be able to get data from the wardrobe microservice. I made view functions to be able to list, create, and delete hats. We made the data accessible in their respective poller function. We made react forms for hats and shoes and lists for shoes and hats in react and tried to make them user friendly
