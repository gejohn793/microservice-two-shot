import React from 'react'
import loadData from './index'

export default class ShoeForm extends React.Component {
        state = {
        name: '',
        manufacturer:'',
        modelName:'',
        color:'',
        pictureUrl:'',
        bin: '',
        bins:[],
        }

    handleNameChange = (event)=>{
        const value = event.target.value
        this.setState({name:value})}


    handleMFGChange = (event)=>{
        const value = event.target.value
        this.setState({manufacturer:value})}

    handleModelNameChange = (event)=>{
        const value = event.target.value
        this.setState({modelName:value})
    }

    handleColorChange = (event)=>{
        const value = event.target.value
        this.setState({color:value})}

    handlePictureUrlChange = (event)=>{
        const value = event.target.value
        this.setState({pictureUrl:value})}

    handleBinChange = (event)=>{
        const value = event.target.value
        this.setState({bin:value})}


    handleFormSubmit = async (event) => {
        event.preventDefault()
        const data = {...this.state}
        data.picture_url = data.pictureUrl
        data.model_name = data.modelName
        delete data.pictureUrl
        delete data.modelName
        delete data.bins


        const shoeUrl = `http://localhost:8080/api/shoes/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }

        const response = await fetch(shoeUrl, fetchConfig)

        if (response.ok){
            const newShoe = await response.json()
            console.log(newShoe)

            const cleared ={
                name: '',
                manufacturer:'',
                modelName:'',
                color:'',
                pictureUrl:'',
                bin: '',
            }
            this.setState(cleared)
            loadData()
    }
        else{
            console.log("An error has occurred")
        }
}
    async componentDidMount(){
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            this.setState({bins:data.bins})
        }
    }


render(){
    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new shoe</h1>
            <form onSubmit = {this.handleFormSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange ={this.handleNameChange} value = {this.state.name}  placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Shoe name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleMFGChange} value = {this.state.manufacturer} placeholder="Shoe Model Name" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="modelName">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleModelNameChange} value = {this.state.modelName} placeholder="Shoe Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="modelName">Shoe Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handleColorChange} value = {this.state.color} placeholder="Color" type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="company_name">Shoe Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange ={this.handlePictureUrlChange} value = {this.state.pictureUrl} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange ={this.handleBinChange} value = {this.state.bin} required id="bin" name= "bin" className="form-select">
                  <option value="">Choose a bin</option>
                    {this.state.bins.map(bin => {
                      return (
                        <option key={bin.id} value={bin.bin_number}>
                          {bin.closet_name}
                        </option>
                      )
                    })};
                </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}















    }
