import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// async function loadShoes() {
//   const response = await fetch('http://localhost:8080/api/shoes/');
//   console.log(response)
//   if (response.ok){
//     const data = await response.json()
//     root.render(
//       <React.StrictMode>
//         <App shoes={data.shoes}/>
//         console.log(shoes)
//       </React.StrictMode>
//     )
//   } else{
//     console.error(response)
//   }
// }

// async function loadHats() {
//   const response = await fetch('http://localhost:8090/api/hats/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadHats()
// loadShoes()

async function loadData() {
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');
  console.log(hatsResponse)
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  console.log(shoeResponse)

  if (hatsResponse.ok && shoeResponse.ok) {
    const hatsData = await hatsResponse.json();
    console.log(hatsData)
    const shoeData = await shoeResponse.json();
    console.log(shoeData)

    root.render(
      <React.StrictMode>
        <App
          hats={hatsData.hats}
          shoes={shoeData.shoes}
        />
      </React.StrictMode>
     )} else {
    console.error(hatsResponse);
    console.error(shoeResponse)
  }
}

loadData()

export default loadData
