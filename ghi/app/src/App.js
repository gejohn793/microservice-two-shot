import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';
import HatsList from './HatsList';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm'
import ShoesList from './ShoesList'


function App(props) {
  console.log(props)
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/new" element={<ShoeForm/>}/>
          <Route path="shoes" element={<ShoesList shoes={props.shoes} />}/>
          <Route path="hats/new" element={<HatForm />} />
          <Route path="hats" element={<HatsList hats={props.hats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
