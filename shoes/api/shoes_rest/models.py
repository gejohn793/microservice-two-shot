from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200,unique = True)

    def __str__(self):
        return f"{self.id}, {self.closet_name}, {self.bin_number}"

class Shoe(models.Model):
    name = models.CharField(max_length = 100)
    manufacturer = models.CharField(max_length =100)
    model_name = models.CharField(max_length = 100)
    color = models.CharField(max_length = 100)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name = "shoes",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_shoe_details", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.id}, {self.name}"
