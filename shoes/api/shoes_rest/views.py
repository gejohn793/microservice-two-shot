from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
        ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "name",
        "picture_url"
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "model_name",
        "color",
        "picture_url"
    ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_number = content["bin"]
            bin = BinVO.objects.get(bin_number=bin_number)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe = False
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"Note": "Shoe does not exist"})
